@echo off

SET GHW_DISABLE_WARNINGS=1

go build .

dotgo.exe test

del build\dotgo.exe >nul 2>&1
move dotgo.exe build\dotgo.exe >nul 2>&1