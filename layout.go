package main

import (
	"fmt"
	"math"
	"os"
	"strconv"
	"sync"

	"gitlab.com/milan44/gopartition"
)

var randomCoordinates map[float64]bool
var coordinateMutex = sync.Mutex{}

func initLayout() {
	randomCoordinates = make(map[float64]bool)
}

func getRandomCoordsForLayout(size float64, graph Graph) (float64, float64) {
	if layout == "random" {
		coordinateMutex.Lock()
		x, y := randomCoords(size, graph)
		coordinateMutex.Unlock()
		return x, y
	} else if layout == "candidate" {
		coordinateMutex.Lock()
		cAttempts = 0
		x, y := candidateCoords(size, graph)
		coordinateMutex.Unlock()
		return x, y
	}
	panic("unknown layout '" + layout + "'")
}

// Point ...
type Point struct {
	x           float64
	y           float64
	size        float64
	avgDistance float64
	overlaps    bool
}

func (p Point) intersects(point Point) bool {
	return intersect(p.x, p.y, p.size, point.x, point.y, point.size)
}
func (p Point) distance(point Point) float64 {
	return distance(point, p)
}
func distance(point1 Point, point2 Point) float64 {
	res := math.Sqrt(pow2(point1.x-point2.x) + pow2(point1.y-point2.y))
	if res < 0 {
		return -res
	}
	return res
}

var candidateVolume *gopartition.Volume

var cAttempts = 0

const samplePointSize = 30

var partitionSize float64 = 5000

func candidateCoords(size float64, graph Graph) (float64, float64) {
	cAttempts++

	maxX, maxY := calculateMinXMinY(size, graph)

	if candidateVolume == nil {
		point := Point{
			x:           randFloat(maxX) + size,
			y:           randFloat(maxY) + size,
			size:        size,
			avgDistance: 0,
		}
		if graph.biggestNodeSize*2 > partitionSize {
			partitionSize = graph.biggestNodeSize * 2
		}
		v := gopartition.NewVolume(partitionSize, fWidth, fHeight)
		candidateVolume = &v

		candidateVolume.Insert(gopartition.Coordinate{
			X:    point.x,
			Y:    point.y,
			Data: point,
		})

		return point.x, point.y
	}
	var wg sync.WaitGroup
	wg.Add(samplePointSize)

	samplePoints := make([]Point, samplePointSize)
	for index := 0; index < samplePointSize; index++ {
		point := Point{
			x:    randFloat(maxX) + size,
			y:    randFloat(maxY) + size,
			size: size,
		}
		lX, lY := candidateVolume.Coordinates(point.x, point.y)

		go func(point Point, lX int, lY int, index int, wg *sync.WaitGroup) {
			locations := candidateVolume.Surrounding(lX, lY)
			overlapping := false
			for _, cPoints := range locations {
				for _, po := range cPoints {
					p := po.Data.(Point)

					if point.intersects(p) || overlapping {
						overlapping = true
						break
					}
				}
			}

			point.overlaps = overlapping

			samplePoints[index] = point

			wg.Done()
		}(point, lX, lY, index, &wg)
	}

	wg.Wait()

	bestPoint := samplePoints[0]
	for _, point := range samplePoints {
		if bestPoint.overlaps {
			bestPoint = point
		}
	}

	if bestPoint.overlaps {
		if cAttempts > 1000 {
			fmt.Println("Error: maximum of 1000 attempts exceeded, please increase canvas size")
			printMinimumCanvasSize(graph)
			os.Exit(1)
		}
		return candidateCoords(size, graph)
	}
	candidateVolume.Insert(gopartition.Coordinate{
		X:    bestPoint.x,
		Y:    bestPoint.y,
		Data: bestPoint,
	})

	return bestPoint.x, bestPoint.y
}

func randomCoords(size float64, graph Graph) (float64, float64) {
	maxX, maxY := calculateMinXMinY(size, graph)

	x := randFloat(maxX) + size
	y := randFloat(maxY) + size
	index := x + fWidth*y

	for randomCoordinates[index] {
		x = randFloat(maxX) + size
		y = randFloat(maxY) + size
		index = x + fWidth*y
	}

	randomCoordinates[index] = true

	return x, y
}

func calculateMinXMinY(size float64, graph Graph) (float64, float64) {
	maxX := (fWidth - size) - size
	maxY := (fHeight - size) - size

	if maxX <= 0 && maxY > 0 {
		fmt.Println("Error: canvas width is too small")
		printMinimumCanvasSize(graph)
		os.Exit(1)
	} else if maxY <= 0 && maxX > 0 {
		fmt.Println("Error: canvas height is too small")
		printMinimumCanvasSize(graph)
		os.Exit(1)
	} else if maxY <= 0 && maxX <= 0 {
		fmt.Println("Error: canvas width and height are too small")
		printMinimumCanvasSize(graph)
		os.Exit(1)
	}

	return maxX, maxY
}
func printMinimumCanvasSize(graph Graph) {
	size := minimumCanvasSize(graph.nodeSizes)
	fmt.Println("Minimum calculated canvas size is " + strconv.Itoa(size) + "x" + strconv.Itoa(size) + " (if your canvas size is bigger than the minimum size, try increasing it more)")
}
func minimumCanvasSize(nodes map[string]float64) int {
	var totalArea float64
	for _, node := range nodes {
		totalArea += pow2(node)

	}
	size := math.Ceil(math.Sqrt(totalArea))

	return int(size)
}
