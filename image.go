package main

import (
	"bytes"
	"fmt"
	"strconv"
	"sync"

	"gitlab.com/milan44/svgo"
)

func renderGraphToSVG(graph Graph, test bool) (int, string) {
	svg := svgo.Image{}
	svg.Init(width, height)

	svg.Rect(0, 0, width, height, "#ffffff", "", 0)

	if showLabels {
		svg.AddCustomSource(`<style>
	.circle {
		background-color: black;
		height: 100%;
		border-radius: 50%;
		text-align: center;
		text-shadow: 0px 0px 2px #AFAFAF;
		word-wrap: anywhere;
		font-family: monospace;
	}
	.circle span {
		line-height: normal;
		display: inline-block;
		vertical-align: middle;
		color: white;
	}
</style>`)
	}

	if !test {
		fmt.Print("Chunkifying Connections...\r")
	}

	chunks := make([][]string, 0)
	for i := 0; i < len(graph.connectionKeys); i += chunkSize {
		end := i + chunkSize

		if end > len(graph.connectionKeys) {
			end = len(graph.connectionKeys)
		}

		chunks = append(chunks, graph.connectionKeys[i:end])
	}
	var wg sync.WaitGroup
	wg.Add(len(chunks))

	if !test {
		fmt.Print("Drawing Connections...          \r")
	}

	connectionCount := 0

	for _, chunk := range chunks {
		go func(connections []string, knownConnections map[string]map[string]float64, nodes map[string]GraphNode, wg *sync.WaitGroup) {
			tCount := 0
			for _, from := range connections {
				toConnections := knownConnections[from]

				if nodes[from] == emptyGraphNode {
					continue
				}
				fromNode := nodes[from]

				for to, weight := range toConnections {
					if nodes[to] == emptyGraphNode {
						continue
					}
					toNode := nodes[to]

					col := "#000000"
					if showHeatmap {
						col = graph.color(weight, 1, graph.biggestConnectionSize-1)
					}

					svg.Line(int(fromNode.x), int(fromNode.y), int(toNode.x), int(toNode.y), line, col)
					tCount++
				}
			}
			connectionCount += tCount
			wg.Done()
		}(chunk, graph.connections, graph.nodes, &wg)
	}

	wg.Wait()

	if !test {
		fmt.Print("Drawing Nodes...                    \r")
	}

	var svgMutex sync.Mutex
	wg.Add(len(graph.nodes))

	for id, node := range graph.nodes {
		go func(nodeSize float64) {
			radius := 1 * fZoom
			if nodeSize != 0 {
				radius = nodeSize * fZoom
			}

			if showLabels {
				r := strconv.Itoa(int(radius))
				w := strconv.Itoa(int(radius) * 2)
				x := strconv.Itoa(int(node.x))
				y := strconv.Itoa(int(node.y))

				if node.label == "commiefornia [6]" {
					fmt.Println(node)
				}

				buf := bytes.Buffer{}
				buf.WriteString("<foreignObject width=\"")
				buf.WriteString(w)
				buf.WriteString("\" height=\"")
				buf.WriteString(w)
				buf.WriteString("\" x=\"")
				buf.WriteString(x)
				buf.WriteString("\" y=\"")
				buf.WriteString(y)
				buf.WriteString("\" transform=\"translate(-")
				buf.WriteString(r)
				buf.WriteString(", -")
				buf.WriteString(r)
				buf.WriteString(")\"><div class=\"circle\" style=\"line-height: ")
				buf.WriteString(w)
				buf.WriteString("px;background-color: ")
				buf.WriteString(node.color)
				buf.WriteString(";font-size: ")
				buf.WriteString(strconv.Itoa(node.fontsize))
				buf.WriteString("px\" xmlns=\"http://www.w3.org/1999/xhtml\"><span>")
				buf.WriteString(node.label)
				buf.WriteString("</span></div></foreignObject>\n")

				str := string(bytes.ReplaceAll(buf.Bytes(), []byte{0}, nil))

				svgMutex.Lock()
				svg.AddCustomSource(str)
				svgMutex.Unlock()
			} else {
				svgMutex.Lock()
				svg.Circle(int(node.x), int(node.y), int(radius), node.color, "", 0)
				svgMutex.Unlock()
			}

			wg.Done()
		}(graph.nodeSizes[id])
	}
	wg.Wait()

	if !test {
		fmt.Print("Rendering SVG...          \r")
	}
	return connectionCount, svg.Render(optimizeSVG)
}
