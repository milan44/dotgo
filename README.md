![badge](https://milan44.gitlab.io/dotgo/test.svg) ![forthebadge](https://forthebadge.com/images/badges/made-with-go.svg) ![forthebadge](https://forthebadge.com/images/badges/made-with-crayons.svg)

![Banner](data/dotgo_banner.png)

DotGo is a software for rendering massive networks very very quickly. So fast, Blink and you might miss it!

---

### Downloads

[Windows](https://milan44.gitlab.io/dotgo/dotgo.exe)  
[Linux](https://milan44.gitlab.io/dotgo/dotgo)

### Syntax
```
[width=50,height=50,zoom=5,line=1,layout=candidate,maxsize=10,optimize=true,renderlabels=false,heatmap=false]
# This is a comment
# Comments can be everywhere except for the first line
# The first line always has to be the renderer arguments
# Comments also have to be on their own line
id [label="mylabel"]
id2 [label="myotherlabel"]
id -- id2
id3 [label="mythirdlabel"]
id -- id3
id3 -- id2
```
Will render the following svg in a few microseconds  
![exampleGraph](data/exampleGraph.svg)

#### width, height
The width and height of the rendered svg (in pixels).

#### zoom
All the edges and nodes sizes will be multiplied by this factor.

#### line
The edge size (in pixels).

#### layout
The layout option can be "random" or "candidate".

#### maxsize
The maximum size nodes should be (in pixels before zoom is applied). Don't set or set to 0 if you don't want a max size.

#### optimize
If the SVG should be optimized (no duplicate lines, etc.). This will take a bit longer though.

#### renderlabels
If the given label should be displayed on top of each node.

#### heatmap
Will color the nodes and connections based on their weight (highest = red, lowest blue).

---

It is able to parse and render (not including reading and writing) 13000 nodes and 82500 connections with the "random" layout in about 130 milliseconds.  
![average](data/average.png)

13000 nodes and 82500 connections look like this  
![bigGraph](data/bigGraph.png)
