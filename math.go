package main

import (
	"math"
	"math/rand"
)

func intersect(x0 float64, y0 float64, r0 float64, x1 float64, y1 float64, r1 float64) bool {
	return pow2(x0-x1)+pow2(y0-y1) <= pow2(r0+r1+2)
	//return pow2(r0+r1) <= pow2(x0+x1)+pow2(y0+y1)
}

func pow2(x float64) float64 {
	return x * x
}

func randFloat(max float64) float64 {
	return math.Floor(rand.Float64() * max)
}
