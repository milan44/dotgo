package main

import (
	"bytes"
	crypto_rand "crypto/rand"
	"encoding/binary"
	"io/ioutil"
	"math/rand"
	"strings"
)

func concat(strs ...string) string {
	buf := bytes.Buffer{}
	for _, s := range strs {
		buf.WriteString(s)
	}
	return buf.String()
}

func readLines(file string) []string {
	byteContents, err := ioutil.ReadFile(file)
	if err != nil {
		panic(err)
	}
	contents := string(byteContents)
	return strings.Split(contents, "\n")
}

func writeFile(file string, contents string) {
	err := ioutil.WriteFile(file, []byte(contents), 0777)
	if err != nil {
		panic(err)
	}
}

func seedRandomGenerator() {
	var b [8]byte
	_, err := crypto_rand.Read(b[:])
	if err != nil {
		panic("cannot seed math/rand package with cryptographically secure random number generator")
	}
	rand.Seed(int64(binary.LittleEndian.Uint64(b[:])))
}
