package main

import (
	"fmt"
	"math"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"
)

// GraphNode ...
type GraphNode struct {
	id        string
	label     string
	x         float64
	y         float64
	radius    float64
	logRadius float64
	color     string
	fontsize  int
}

// Graph ...
type Graph struct {
	nodes                 map[string]GraphNode
	nodeSizes             map[string]float64
	connections           map[string]map[string]float64
	connectionKeys        []string
	biggestNodeSize       float64
	biggestConnectionSize float64
}

func (g Graph) color(w, min, max float64) string {
	normalized := w - min
	proc := (normalized / max) * 240
	col := 240 - proc

	if col < 0 {
		col = 0
	}

	return "hsl(" + strconv.Itoa(int(col)) + ", 100%, 50%)"
}

func parseGraph(lines []string) Graph {
	graph := Graph{
		nodes:          make(map[string]GraphNode),
		nodeSizes:      make(map[string]float64),
		connections:    make(map[string]map[string]float64),
		connectionKeys: make([]string, 0),
	}
	nodeMutex := sync.Mutex{}
	nodeSizeMutex := sync.Mutex{}
	connectionMutex := sync.Mutex{}

	chunks := make([][]string, 0)
	for i := 0; i < len(lines); i += parseChunkSize {
		end := i + parseChunkSize

		if end > len(lines) {
			end = len(lines)
		}

		chunks = append(chunks, lines[i:end])
	}
	var wg sync.WaitGroup
	wg.Add(len(chunks))

	for _, chunk := range chunks {
		go func(graph *Graph, lines []string, wg *sync.WaitGroup) {
			for _, line := range lines {
				words := strings.Split(strings.TrimSpace(line), " ")
				if strings.HasPrefix(line, "#") || strings.HasPrefix(line, "[") || len(words) < 1 {
					continue
				}

				if len(words) > 1 && words[1] == "--" {
					connectionMutex.Lock()
					connections, ok := graph.connections[words[0]]

					if !ok {
						c2, ok2 := graph.connections[words[2]]

						if ok2 {
							t := words[0]
							words[0] = words[2]
							words[2] = t

							connections = c2
						} else {
							connections = make(map[string]float64)

							graph.connectionKeys = append(graph.connectionKeys, words[0])
						}
					}

					connections[words[2]]++

					if connections[words[2]] > graph.biggestConnectionSize {
						graph.biggestConnectionSize = connections[words[2]]
					}

					graph.connections[words[0]] = connections
					connectionMutex.Unlock()

					nodeSizeMutex.Lock()
					graph.nodeSizes[words[0]]++
					graph.nodeSizes[words[2]]++
					nodeSizeMutex.Unlock()
				} else {
					label := ""
					if len(words) > 1 {
						label = parseArguments(words[1])["label"]
					}

					nodeMutex.Lock()
					graph.nodes[words[0]] = GraphNode{
						id:    words[0],
						label: label,
					}
					nodeMutex.Unlock()
				}
			}
			wg.Done()
		}(&graph, chunk, &wg)
	}
	wg.Wait()

	scale := (fWidth / 1000) / 2
	if scale < 1 {
		scale = 1
	}

	biggestOriginal := 0.0
	smallestOriginal := math.MaxFloat64
	for id, size := range graph.nodeSizes {
		logSize := math.Log(size) * scale
		if logSize > graph.biggestNodeSize {
			graph.biggestNodeSize = logSize
		}

		n := graph.nodes[id]
		n.logRadius = logSize
		graph.nodes[id] = n

		if size > biggestOriginal {
			biggestOriginal = size
		}
		if size < smallestOriginal {
			smallestOriginal = size
		}
	}

	max := biggestOriginal - smallestOriginal

	nodeArray := make([]GraphNode, 0)
	for _, node := range graph.nodes {
		nodeArray = append(nodeArray, node)
	}
	nodeChunks := chunkifyGraphNodeArray(nodeArray)
	var waitGroup sync.WaitGroup
	waitGroup.Add(len(nodeChunks))

	orderedNodes := make([][]GraphNode, int(graph.biggestNodeSize+1)*zoom)
	orderedNodesMutex := sync.Mutex{}

	for _, chunk := range nodeChunks {
		go func(graph *Graph, nodes []GraphNode, waitGroup *sync.WaitGroup) {
			for _, node := range nodes {
				nodeSizeMutex.Lock()
				size := graph.nodeSizes[node.id]
				nodeSizeMutex.Unlock()

				if showHeatmap {
					node.color = graph.color(size, smallestOriginal, max)
				} else {
					node.color = "#000000"
				}

				if showLabels {
					node.label += " [" + strconv.Itoa(int(size)) + "]"
				}

				radius := 1 * fZoom * scale

				if node.logRadius > 0 {
					radius = node.logRadius * fZoom
				}

				node.radius = radius
				intRadius := int(radius)

				if showLabels {
					radius -= radius * 0.12

					a := math.Pi * (radius * radius)
					c := len([]rune(node.label))
					node.fontsize = int(math.Sqrt(a / float64(c)))

					node.label = strings.ReplaceAll(node.label, "<", "&lt;")
					node.label = strings.ReplaceAll(node.label, ">", "&gt;")
				}

				orderedNodesMutex.Lock()
				orderedNodes[intRadius] = append(orderedNodes[intRadius], node)
				orderedNodesMutex.Unlock()
			}
			waitGroup.Done()
		}(&graph, chunk, &waitGroup)
	}
	waitGroup.Wait()

	nodeArray = make([]GraphNode, 0)
	for i := len(orderedNodes) - 1; i >= 0; i-- {
		nodeArray = append(nodeArray, orderedNodes[i]...)
	}
	nodeChunks = chunkifyGraphNodeArray(nodeArray)
	var waitGroup2 sync.WaitGroup
	waitGroup2.Add(len(nodeChunks))

	for _, chunk := range nodeChunks {
		go func(graph *Graph, nodes []GraphNode, waitGroup2 *sync.WaitGroup) {
			for _, node := range nodes {
				x, y := getRandomCoordsForLayout(node.radius, *graph)

				node.x = x
				node.y = y

				nodeMutex.Lock()
				graph.nodes[node.id] = node
				nodeMutex.Unlock()
			}
			waitGroup2.Done()
		}(&graph, chunk, &waitGroup2)
	}
	waitGroup2.Wait()

	//checkOverlaps(graph.nodes)

	return graph
}

func checkOverlaps(nodes map[string]GraphNode) {
	fmt.Println("Checking overlaps...")
	var wg sync.WaitGroup
	wg.Add(len(nodes))
	allOverlapps := make([][]string, len(nodes))
	i := 0
	for id, node := range nodes {
		go func(id string, node GraphNode, nodes map[string]GraphNode, i int) {
			tempOverlaps := make([]string, 0)

			point := Point{
				x:    node.x,
				y:    node.y,
				size: node.radius,
			}
			for oID, oNode := range nodes {
				if oID == id {
					continue
				}
				oPoint := Point{
					x:    oNode.x,
					y:    oNode.y,
					size: oNode.radius,
				}

				distance := point.distance(oPoint) - (oPoint.size + point.size)
				if distance < 0 {
					t1 := fmt.Sprint(partitionLocationForXY(oPoint.x, oPoint.y))
					t2 := fmt.Sprint(partitionLocationForXY(point.x, point.y))

					tempOverlaps = append(tempOverlaps, t1+" - "+t2, t2+" - "+t1)
				}
			}

			allOverlapps[i] = tempOverlaps
			wg.Done()
		}(id, node, nodes, i)
		i++
	}

	cleanOverlaps := make(map[string]bool)
	count := 0
	for _, l := range allOverlapps {
		for _, lap := range l {
			if !cleanOverlaps[lap] {
				cleanOverlaps[lap] = true
				fmt.Println(lap)
				count++
			}
		}
	}
	wg.Wait()
	fmt.Println("Done checking overlaps! (" + strconv.Itoa(count/2) + " overlapping nodes found)")
	fmt.Println(partitionSize)
	os.Exit(0)
}
func partitionLocationForXY(x float64, y float64) (int, int) {
	px := math.Floor(x / partitionSize)
	py := math.Floor(y / partitionSize)

	return int(px), int(py)
}

func chunkifyGraphNodeArray(nodeArray []GraphNode) [][]GraphNode {
	nodeChunks := make([][]GraphNode, 0)
	for i := 0; i < len(nodeArray); i += nodeChunkSize {
		end := i + nodeChunkSize

		if end > len(nodeArray) {
			end = len(nodeArray)
		}

		nodeChunks = append(nodeChunks, nodeArray[i:end])
	}
	return nodeChunks
}

func parseArguments(args string) map[string]string {
	var re = regexp.MustCompile(`(?mU)([^\[]+)=(.+)[,\]]`)
	matches := re.FindAllStringSubmatch(args, 100)
	arguments := make(map[string]string)
	for _, match := range matches {
		arguments[match[1]] = strings.ReplaceAll(match[2], "\"", "")
	}
	return arguments
}
