package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"runtime"
	"runtime/pprof"
	"strconv"

	"github.com/jaypipes/ghw"
)

// UsageInfo ...
type UsageInfo struct {
	cpuProfile *os.File
	memProfile *os.File
}

func optimizeSystemConfig() {
	cpuCount := runtime.NumCPU()
	runtime.GOMAXPROCS(cpuCount)
}

func startUsageMonitor(usage *UsageInfo) {
	cpuProfile, err := os.Create("__dotgo_cpuprofile.prof")
	if err != nil {
		panic("could not create CPU profile: " + err.Error())
	}
	memProfile, err := os.Create("__dotgo_memprofile.prof")
	if err != nil {
		panic("could not create Memory profile: " + err.Error())
	}

	if err := pprof.StartCPUProfile(cpuProfile); err != nil {
		panic("could not start CPU profile: " + err.Error())
	}

	usage.cpuProfile = cpuProfile
	usage.memProfile = memProfile
}
func stopUsageMonitor(usage *UsageInfo) {
	fmt.Print("Exporting profiles...")

	runtime.GC()

	pprof.StopCPUProfile()
	if err := pprof.WriteHeapProfile(usage.memProfile); err != nil {
		panic("could not write memory profile: " + err.Error())
	}

	usage.memProfile.Close()
	usage.cpuProfile.Close()

	cmd := exec.Command("go", "tool", "pprof", "-tree", "__dotgo_memprofile.prof")
	buf := bytes.Buffer{}
	cmd.Stderr = &buf
	memory, err := cmd.Output()
	if err != nil {
		fmt.Println(buf.String())
		panic(err)
	}
	
	ioutil.WriteFile("dotgo-memory-profile.txt", memory, 0777)

	cmd = exec.Command("go", "tool", "pprof", "-tree", "__dotgo_cpuprofile.prof")
	buf = bytes.Buffer{}
	cmd.Stderr = &buf
	cpu, err := cmd.Output()
	if err != nil {
		fmt.Println(buf.String())
		panic(err)
	}
	
	ioutil.WriteFile("dotgo-cpu-profile.txt", cpu, 0777)

	os.Remove("__dotgo_memprofile.prof")
	os.Remove("__dotgo_cpuprofile.prof")

	fmt.Println("Done")
}

func printConfig() {
	indent := "  "
	info := "\nConfiguration:\n"

	info += indent + "Width: " + strconv.Itoa(width) + "\n"
	info += indent + "Height: " + strconv.Itoa(height) + "\n"
	info += indent + "Zoom: " + strconv.Itoa(zoom) + "\n"
	info += indent + "Line-Width: " + strconv.Itoa(line) + "\n"
	info += indent + "Layout: " + layout + "\n"

	fmt.Println(info)
}
func printSystemInfo() {
	indent := "  "
	systemInfo := "System Information:\n"

	systemInfo += indent + "Memory:\n"
	memory, err := ghw.Memory()
	if err != nil {
		systemInfo += indent + indent + "Failed to get memory info\n"
	} else {
		systemInfo += indent + indent + "(" + formatByteSize(memory.TotalPhysicalBytes) + " physical, " + formatByteSize(memory.TotalUsableBytes) + " usable)\n"
		for _, module := range memory.Modules {
			systemInfo += indent + indent + "[" + module.Vendor + "] " + formatByteSize(module.SizeBytes) + "\n"
		}
	}

	systemInfo += indent + "CPU(s):\n"
	cpu, err := ghw.CPU()
	if err != nil {
		systemInfo += indent + indent + "Failed to get cpu info\n"
	} else {
		systemInfo += indent + indent + "(" + fmt.Sprintf("%v", cpu.TotalCores) + " total cores, " + fmt.Sprintf("%v", cpu.TotalThreads) + " total threads)\n"
		for _, proc := range cpu.Processors {
			systemInfo += indent + indent + "[" + proc.Vendor + "] " + proc.Model + " (" + fmt.Sprintf("%v", proc.NumCores) + " cores, " + fmt.Sprintf("%v", proc.NumThreads) + " threads)\n"
		}
	}

	systemInfo += indent + "GPU(s):\n"
	gpu, err := ghw.GPU()
	if err != nil {
		systemInfo += indent + indent + "Failed to get gpu info\n"
	} else {
		for _, card := range gpu.GraphicsCards {
			if card.DeviceInfo == nil {
				continue
			}
			systemInfo += indent + indent + "[" + card.DeviceInfo.Vendor.Name + "] " + card.DeviceInfo.Product.Name + "\n"
		}
		if len(gpu.GraphicsCards) == 0 {
			systemInfo += indent + indent + "none found"
		}
	}

	fmt.Println(systemInfo)
}
func formatByteSize(b int64) string {
	const unit = 1024
	if b < unit {
		return fmt.Sprintf("%d B", b)
	}
	div, exp := int64(unit), 0
	for n := b / unit; n >= unit; n /= unit {
		div *= unit
		exp++
	}
	return fmt.Sprintf("%.1f %ciB",
		float64(b)/float64(div), "KMGTPE"[exp])
}
