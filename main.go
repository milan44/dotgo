package main

import (
	"fmt"
	"math"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"
)

var inputFile = "data/graph.dot"
var outputFile = "data/graph"

const parseChunkSize = 8000
const chunkSize = 4000
const nodeChunkSize = 10000

var width = 50
var height = 50
var zoom = 5
var maxNodeSize float64 = 0
var optimizeSVG = true
var showLabels = true
var showHeatmap = false
var line = 1
var layout = "random"
var extension = "svg"

var fWidth = float64(width)
var fHeight = float64(height)
var fZoom = float64(zoom)

func main() {
	optimizeSystemConfig()

	args := os.Args[1:]
	if len(args) > 0 {
		if args[0] == "test" {
			monitor := UsageInfo{}
			startUsageMonitor(&monitor)

			totalTime := float64(0)
			inputFile = "data/test.dot"
			outputFile += "." + extension

			lines := readLines(inputFile)
			for i := 0; i < 100; i++ {
				if i > 0 {
					reset()
					runtime.GC()
				}
				fmt.Print("Running benchmark " + strconv.Itoa(i+1) + " of 100\r")
				totalTime += run(true, lines)
			}
			average := math.Round(totalTime / 100)
			fmt.Println("Benchmarking finished, average parse and render time after 100 runs is " + fmtNanoSeconds(average))
			printConfig()
			printSystemInfo()

			stopUsageMonitor(&monitor)
		} else {
			inputFile = args[0]
			if len(args) > 1 {
				outputFile = args[1]
			} else {
				outputFile = strings.ReplaceAll(inputFile, ".dot", "") + "." + extension
			}

			run(false, []string{})
		}
	} else {
		fmt.Println("You need to specify an input file!")
	}
}

func fmtNanoSeconds(nanoSec float64) string {
	microSec := math.Floor(nanoSec / 1000)
	nanoSec -= microSec * 1000

	milliSec := math.Floor(microSec / 1000)
	microSec -= milliSec * 1000

	seconds := math.Floor(milliSec / 1000)
	milliSec -= seconds * 1000

	t := make([]string, 0)

	if seconds > 0 {
		t = append(t, strconv.Itoa(int(seconds))+" seconds")
	}
	if milliSec > 0 {
		t = append(t, strconv.Itoa(int(milliSec))+" milliseconds")
	}
	if microSec > 0 {
		t = append(t, strconv.Itoa(int(microSec))+" microseconds")
	}
	t = append(t, strconv.Itoa(int(nanoSec))+" nanoseconds")

	fmtTime := strings.Join(t, ", ")
	if len(t) > 1 {
		index := strings.LastIndex(fmtTime, ", ")
		fmtTime = fmtTime[:index] + " and" + fmtTime[index+1:]
	}
	return fmtTime
}

func setArgumentIfDefined(args map[string]string, key string, val int) int {
	if args[key] != "" {
		s, err := strconv.Atoi(args[key])
		if err == nil {
			return s
		}
	}
	return val
}
func setStringArgumentIfDefined(args map[string]string, key string, val string) string {
	if args[key] != "" {
		return args[key]
	}
	return val
}
func setBoolArgumentIfDefined(args map[string]string, key string, val bool) bool {
	if args[key] == "true" {
		return true
	} else if args[key] == "false" {
		return false
	}
	return val
}

var emptyGraphNode = GraphNode{}

func run(test bool, lines []string) float64 {
	if !test {
		fmt.Print("Seeding random number generator...\r")
	}
	seedRandomGenerator()

	if !test {
		fmt.Print("Reading file...\r")
	}
	if len(lines) == 0 {
		lines = readLines(inputFile)
	}

	setup := parseArguments(lines[0])

	width = setArgumentIfDefined(setup, "width", width)
	height = setArgumentIfDefined(setup, "height", height)
	line = setArgumentIfDefined(setup, "line", line)
	zoom = setArgumentIfDefined(setup, "zoom", zoom)
	maxNodeSize = float64(setArgumentIfDefined(setup, "maxsize", int(maxNodeSize)))
	layout = setStringArgumentIfDefined(setup, "layout", layout)
	extension = setStringArgumentIfDefined(setup, "extension", extension)
	optimizeSVG = setBoolArgumentIfDefined(setup, "optimize", optimizeSVG)
	showLabels = setBoolArgumentIfDefined(setup, "renderlabels", showLabels)
	showHeatmap = setBoolArgumentIfDefined(setup, "heatmap", showHeatmap)

	//height *= zoom
	//width *= zoom
	line *= zoom

	fWidth = float64(width)
	fHeight = float64(height)
	fZoom = float64(zoom)

	initLayout()

	now := time.Now()
	startTime := now.UnixNano()

	if !test {
		fmt.Print("Parsing Graph...                  \r")
	}
	graph := parseGraph(lines)
	lines = nil

	connectionCount := 0

	if extension == "svg" {
		source := ""
		connectionCount, source = renderGraphToSVG(graph, test)

		if !test {
			fmt.Print("Writing File...          \r")
			writeFile(outputFile, source)
		}
	} else {
		panic("Invalid extension '" + extension + "'")
	}

	now = time.Now()
	endTime := now.UnixNano()

	if !test {
		fmt.Println("Rendered " + strconv.Itoa(len(graph.nodes)) + " nodes and " + strconv.Itoa(connectionCount) + " connections in " + fmtNanoSeconds(float64(endTime-startTime)))
	}

	return float64(endTime - startTime)
}

func reset() {
	candidateVolume = nil
	randomCoordinates = make(map[float64]bool)

	width = 50
	height = 50
	zoom = 5
	line = 1
	layout = "random"

	seedRandomGenerator()
}
